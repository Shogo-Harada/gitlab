import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import ScalarFormatter
#from matplotlib import rc		//これはなくても添え字など可能

#rc('text', usetex=True)

X=np.array([0,1,2,3,4])
Y1=np.array([0,1,4,2,3])
Y2=np.array([1,1.1,4,2,3])


fig=plt.figure(figsize=(6,6))
ax=fig.add_subplot(111)
ax.plot(X,Y1,color="b",marker='o')
ax.plot(X,Y2,color="g",marker='o')

ax.set_title("title",fontsize=30)
ax.set_xlabel("x",fontsize=30)
ax.set_ylabel("y",fontsize=30)
ax.tick_params(labelsize=30)
ax.legend(["data1","data2"],fontsize=30)
ax.set_xticks(np.arange(,,0.01))
ax.grid(axis="both",linestyle='--',minor=False)

ax.xaxis.set_major_formatter(ScalarFormatter(useMathText=True))
ax.yaxis.set_major_formatter(ScalarFormatter(useMathText=True))
ax.ticklabel_format(style="sci",  axis="both",scilimits=(0,0))
ax.xaxis.offsetText.set_fontsize(30)
ax.yaxis.offsetText.set_fontsize(30)




fig.tight_layout()
#fig2.tight_layout()
#fig3.tight_layout()
plt.show()

pdf=PdfPages("plot.pdf")
pdf.savefig(fig)
pdf.close()
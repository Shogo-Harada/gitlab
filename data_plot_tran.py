import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import ScalarFormatter
#from matplotlib import rc		//これはなくても添え字など可能

#rc('text', usetex=True)

T=np.loadtxt("",skiprows=1,usecols=0)
data11=np.loadtxt("",skiprows=1,usecols=1)
data12=np.loadtxt("",skiprows=1,usecols=2)

data21=np.loadtxt("",skiprows=1,usecols=3)
data22=np.loadtxt("",skiprows=1,usecols=4)


fig=plt.figure(figsize=(10,10))
ax=fig.add_subplot(211)
ax.plot(T,data11,color="b")
ax.plot(T,data12,color="g")

ax.set_title("title",fontsize=30)
ax.set_xlabel("time [s]",fontsize=30)
ax.set_ylabel("y",fontsize=30)
ax.tick_params(labelsize=30)
ax.legend(["data1","data2"],fontsize=30)
ax.set_ylim()
ax.grid(axis="both",linestyle='--',minor=False)
ax.xaxis.set_major_formatter(ScalarFormatter(useMathText=True))
ax.ticklabel_format(style="sci",  axis="x",scilimits=(0,0))
ax.xaxis.offsetText.set_fontsize(30)


ax2=fig.add_subplot(212)
ax2.plot(T,data21,color="b")
ax2.plot(T,data22,color="g")
ax2.grid(axis="both",linestyle='--',minor=False)

ax2.set_title("title",fontsize=30)
ax2.set_xlabel("time [s]",fontsize=30)
ax2.set_ylabel("y",fontsize=30)
ax2.tick_params(labelsize=30)
ax2.legend(["data1","data2"],fontsize=30)
ax2.set_ylim()
ax2.xaxis.set_major_formatter(ScalarFormatter(useMathText=True))
ax2.ticklabel_format(style="sci",  axis="x",scilimits=(0,0))
ax2.xaxis.offsetText.set_fontsize(30)

fig.tight_layout()
#fig2.tight_layout()
#fig3.tight_layout()
plt.show()

pdf=PdfPages("plot.pdf")
pdf.savefig(fig)
pdf.close()
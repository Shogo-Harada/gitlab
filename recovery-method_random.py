import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import ScalarFormatter



def add(a,b):		#加算
	if len(a)>len(b):
		b=b+[0]*(len(a)-len(b))
	else:
		a=a+[0]*(len(b)-len(a))
	
	X=[]
	up=0
	for n in range(len(a)):
		if a[n]==0 and b[n]==0 and up==0:
			x=0
			up=0
		elif a[n]==1 and b[n]==0 and up==0:
			x=1
			up=0
		elif a[n]==0 and b[n]==1 and up==0:
			x=1
			up=0
		elif a[n]==1 and b[n]==1 and up==0:
			x=0
			up=1
		elif a[n]==0 and b[n]==0 and up==1:
			x=1
			up=0
		elif a[n]==1 and b[n]==0 and up==1:
			x=0
			up=1
		elif a[n]==0 and b[n]==1 and up==1:
			x=0
			up=1
		elif a[n]==1 and b[n]==1 and up==1:
			x=1
			up=1
		
		X.append(x)
	
	X.append(up)
	
	return X


def comp(X):	#2の補数
	
	X_comp=[]
	for x in X:
		if x==0:
			X_comp.append(1)
		elif x==1:
			X_comp.append(0)
			
	X_comp=add(X_comp,[1])
	del X_comp[len(X_comp)-1]
	
	return X_comp

	
def sub(a,b):	#減算
	
	b_comp=comp(b)
	X=add(a,b_comp)
	
	del X[len(X)-1]
	
	return X

def oct_to_bi(n):
	B=[]
	while n>0:
		B.append(n%2)
		n=n//2
		
	return B	#Bはビット列左右反転

def bi_to_oct(B):	#Bはビット列反転なし
	oct=0
	for n in range(len(B)):
		oct+=B[n]*2**(len(B)-n-1)
		
	return oct
	

Z_list=[]
d_list=[]
ans_method_list=[]
ans_real_list=[]
diff_list=[]

max_digit_Z=10
maz_digit_d=7	
	
random=np.random.randint(0,2**max_digit_Z-1,1000)
random2=np.random.randint(1,2**maz_digit_d-1,1000)

for n in range(1000):
	Z=oct_to_bi(random[n])  #被除数(ビット列左右反転)
	Z.append(0)	#符号ビット
	
	d=oct_to_bi(random2[n])  #被除数(ビット列左右反転)
	d.append(0)	#符号ビット
	

	pq=[]

	Z=Z+([0]*(maz_digit_d-1))
	pr=Z[len(Z)-len(d):len(Z)]		#レジスタpr部
	i=len(Z)-len(d)
	while i>=0:
		pr_next=sub(pr,d)
		
		if pr_next[len(pr_next)-1]==0:
			del pr_next[len(pr_next)-1]
			pq.append(1)
		else:
			pq.append(0)
			pr_next=pr;	#引きすぎた分を戻す
			if pr_next[len(pr_next)-1]==0:
				del pr_next[len(pr_next)-1]
				

		pr_next=[Z[i-1]]+pr_next
		pr=pr_next
		
		i-=1
		
	answer_method=bi_to_oct(pq)	#計算値。結果はビット列反転せず
	
	answer_real=random[n]//random2[n]	#真の値
	
	print("Z=%d\td=%d\t計算値=%d\t新の値=%d" % (random[n],random2[n],answer_method,answer_real))
		
		
	Z_list.append(random[n])
	d_list.append(random2[n])
	ans_method_list.append(answer_method)
	ans_real_list.append(answer_real)
	diff_list.append(answer_method-answer_real)
	
	
fig=plt.figure(figsize=(8,8))
ax=fig.add_subplot(111)
ax.scatter(np.arange(1,1001,1),diff_list)

ax.set_title("calculation error of recovery method division",fontsize=20)
ax.set_xlabel("test number",fontsize=20)
ax.set_ylabel("error (result value - real answer)",fontsize=20)
ax.tick_params(labelsize=15)
ax.set_ylim(-10,10)
ax.grid(axis="both",linestyle='--')

plt.tight_layout()
plt.show()

pdf=PdfPages("recovery_method.pdf")
pdf.savefig(fig)
pdf.close()